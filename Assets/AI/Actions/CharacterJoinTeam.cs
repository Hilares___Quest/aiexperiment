using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CharacterJoinTeam : RAINAction
{
    private PlayerCharacter m_playerCharacter;
    

    public override void Start(RAIN.Core.AI ai)
    {
        m_playerCharacter = ai.GetCustomElement<PlayerCharacter>();

        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        //RAIN.Motion.MoveLookTarget a = new RAIN.Motion.MoveLookTarget();
        //a.ObjectTarget = m_playerCharacter.positionInTeam;
        //Debug.Log("Arrived:" + ai.Motor.IsAt(a));

        //if (ai.Motor.IsAt(a))
        //{
        //    ai.WorkingMemory.SetItem("isOnTeam", true);
        //}
        //else
        //    ai.WorkingMemory.SetItem("isOnTeam", false);

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}