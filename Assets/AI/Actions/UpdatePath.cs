using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class UpdatePath : RAINAction
{
    public float breackDistance = 3f;

    private GameObject m_target;
    private Vector3 m_lastPos = new Vector3(-9999,-9999,-9999);
    private RAIN.Motion.MoveLookTarget m_nextTaget;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        m_target = ai.WorkingMemory.GetItem<GameObject>("target");
        m_lastPos = ai.WorkingMemory.GetItem<Vector3>("lastPos");
        m_nextTaget = ai.WorkingMemory.GetItem<RAIN.Motion.MoveLookTarget>("nextTarget");

        if(m_nextTaget != null && m_nextTaget.TransformTarget != m_target.transform && m_nextTaget.Position != m_lastPos && m_nextTaget.Position != m_target.transform.position)
        {
            m_lastPos = m_nextTaget.Position;
            ai.WorkingMemory.SetItem<bool>("updatePath", true);
            ai.WorkingMemory.SetItem<Vector3>("lastPos", m_lastPos);

            Debug.Log("fuck");
        }


        //if(m_target != null && Vector3.Distance(m_target.transform.position, m_lastPos) > breackDistance)
        //{
        //    m_lastPos = m_target.transform.position;
        //    ai.WorkingMemory.SetItem<bool>("updatePath", true);
        //    ai.WorkingMemory.SetItem<Vector3>("lastPos", m_lastPos);
        //}
        //RAIN.Motion.MoveLookTarget adc = new RAIN.Motion.MoveLookTarget();
        //adc.


        //ai.Navigator.RestartPathfindingSearch();
        //ai.Navigator.CurrentPath = null;
        //Debug.Log("coucou");
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}