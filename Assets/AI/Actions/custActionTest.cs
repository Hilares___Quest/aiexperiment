using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

using RAIN.Serialization;

//[RAINAction]
//public class custActionTest : RAINAction
//{
//    [RAINSerializableField]
//    private GameObject _test;

//    public override void Start(RAIN.Core.AI ai)
//    {
//        base.Start(ai);
//    }

//    public override ActionResult Execute(RAIN.Core.AI ai)
//    {
//        return ActionResult.SUCCESS;
//    }

//    public override void Stop(RAIN.Core.AI ai)
//    {
//        base.Stop(ai);
//    }
//}

[RAINSerializableClass]
public class custActionTest : CustomAIElement
{
    [RAINSerializableField]
    private float _standardFloat = 1.2f;

    [RAINSerializableField(Visibility = FieldVisibility.ShowAdvanced, ToolTip = "An advanced float")]
    private float _advancedFloat = 4.5f;

    public override void AIInit()
    {
        base.AIInit();

        // This is equivilent to an Awake call
    }
}