﻿using UnityEngine;
using System.Collections;

public class NavigationTarget : MonoBehaviour {

    private NavMeshAgent m_agent;
    [SerializeField]
    private Transform m_target;
    [SerializeField]
    private float m_stoppingDistance;

    // Use this for initialization
    void Start () {
        m_agent = GetComponent<NavMeshAgent>();
        m_agent.stoppingDistance = m_stoppingDistance;
	}
	
	// Update is called once per frame
	void Update () {
        m_agent.SetDestination(m_target.position);
        //m_agent.
        m_agent.stoppingDistance = m_stoppingDistance;
    }
}
