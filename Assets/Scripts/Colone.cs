﻿using UnityEngine;
using System.Collections;

public class Colone : MonoBehaviour {

    private Rigidbody m_rigid;
	// Use this for initialization
	void Start () {
        m_rigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Break()
    {
        m_rigid.isKinematic = false;
    }
}
