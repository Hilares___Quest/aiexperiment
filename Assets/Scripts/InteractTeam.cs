﻿using UnityEngine;
using System.Collections.Generic;

public class InteractTeam : MonoBehaviour {

    [SerializeField]
    private RAIN.Core.AIRig m_ai;

    private TeamAI m_aiElement;

    [SerializeField]
    private List<bool> m_active;

    private List<bool> m_last; 

	// Use this for initialization
	void Start () {
        m_aiElement = m_ai.AI.GetCustomElement<TeamAI>();
        m_last = new List<bool>();
        m_last.InsertRange(0, m_active);
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < m_active.Count; i++)
        {
            if(m_active[i] != m_last[i])
            {
                if (m_active[i])
                {
                    m_aiElement.JoinTeam(i);
                    Debug.Log("Join");
                }
                else
                    m_aiElement.LeaveTeam(i);
            }
        }

        m_last.Clear();
        m_last.InsertRange(0, m_active);
    }
}
